﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack_VR1
{
    class Validation
    {
        public Validation()
        {
        }

        public Boolean checkNum(string userNum)
        {
            if (int.TryParse(userNum, out int n))
            {
                return true;
            }

            return false;

        }

        public Boolean checkFunds(int usersCash)
        {
            if (usersCash < 5)
            {
                return false;

            }

            return true;
        }

        public Boolean checkBet(int bet, int usersCash)
        {
            if (bet > usersCash || bet < 20)
            {
                return false;
            }
            return true;
        }

    }
}
