﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;


namespace BlackJack_VR1
{
    class Program
    {
        string[,] cardDeck = new string[52, 3];
        string[,] shuffledDeck = new string[52, 3];
        string[] usedCards = new string[52];
        string [] outPut = {"",""};
        string shuffleLongString = "";

        string centering = "                              ";
        string glossary = "               ";
        
        //specific card locations for the user for the round
        int[,] usersUsedCards = new int[2, 5];
        //lowest 1 highest 5
        int[] numCardsUsed = {0, 0};
        //points in total for the round
        int[] usersHand = { 0, 0 };

        int turn = 0; 
        int drawCard = 0;
        int usersCash = 100;
        int userBet = 0;

        Boolean isGameBet = false;
        Boolean winRound = false;

        XmlDoc document = new XmlDoc();
        Words word = new Words();
        Validation validate = new Validation();
        JSONDoc jsonFile = new JSONDoc();

        SoundPlayer backgroundMusic = new SoundPlayer();
        

        static void Main(string[] args)
        {

            Program program = new Program();
            program.setAssets();
            
        }

        public void setAssets()
        {
            Console.SetWindowSize(Console.WindowWidth, 32);


            backgroundMusic.SoundLocation = @"..\..\Sounds\casino.wav";
            backgroundMusic.PlayLooping();

            fillDeck();

            startGame();
        }

        public void startGame()
        {
            usersCash = 100;
            word.displayGameText();
            Console.WriteLine("\t\t\t\t\t\t WELCOME TO BLACK JACK");
            Console.WriteLine("\t\t The name of the game is simple, you bet to see if you can get as close as possible to 21 while having a higher number than the house (which is me)."
             + "The minimum bet for this table is 20 dollars. You start with 100 dollars. If you win you get your money back as well as the houses money. Some times there will be a round" +
             " where the house puts in a extra offer to triple your money. You can either take it or decline it and play for the regular amount you wanted.");

            Console.WriteLine();
            Boolean isReady = true;

            Console.ForegroundColor = ConsoleColor.DarkGreen;
            Console.Write("\t\t\t\t PLAY: 1");

            Console.ForegroundColor = ConsoleColor.DarkMagenta;
            Console.Write("\t\tLOAD GAME: 2");

            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write("\t\tHIGHSCORE: 3");

            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write("\t\tEXIT: 4");

            Console.WriteLine();
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.White;
            string userInput = "";
            do
            {
              
                Console.Write("\t\t\t\t\t\tWhat Would you like to do: ");

                userInput = Console.ReadLine();
                
                if(userInput != "1" && userInput != "2" && userInput != "3" && userInput != "4")
                {
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    RemoveCurrentConsoleLine();
                    isReady = false;

                }
                else
                {
                    isReady = true;
                }

            } while (isReady == false);


            switch (userInput)
            {
                case "1":
                    resetForNewGame();
                    //play game
                    Console.Clear();
                    makeBet();
                    chooseCard();
                    chooseCard();
                    startRound();
                    break;
                case "2":
                    //load a saved game
                    jsonFile.loadJsonFile();
                    checkSave();
                    break;
                case "3":
                    //see highscore
                    loadHighScore();
                    break;
                case "4":
                    //close program
                    Environment.Exit(0);
                    break;
            }

           
        }

        public void loadHighScore()
        {
            Console.Clear();
            word.displayGameText();
            word.displayHighScoreText();

            Console.WriteLine("\t\t\t\t\t\tTHE HIGHSCORE TO BEAT IS: $" + document.getHighScore());
            Console.WriteLine("\t\t\t\t      THE HIGHEST BET EVER MADE IN A ROUND AND WON IS: $" + document.getHighestBet());

            Console.WriteLine();
            Console.WriteLine("\t\t\t\t\t  Press enter to go back to the main menu...");
            Console.ReadLine();
            Console.WriteLine();

            Console.Clear();
            startGame();

        }

        public void checkSave()
        {
            if (jsonFile.checkSaveFile())
            {
                //loadSave
                string saveType = jsonFile.getSaveType();

                loadSave(saveType);
            }
            else
            {
                Console.Clear();

                word.displayNoSaveText();
                Console.WriteLine();
                Console.WriteLine("\t\t\t\t\t\tNO SAVE GAME FOUND!");

                Thread.Sleep(2500);

                Console.Clear();
                startGame();
            }
        }

        public void loadSave(string saveType)
        {
            switch (saveType)
            {
                case "1":
                    jsonFile.loadJsonFile();
                    jsonFile.loadJsonObject();
                    loadDeck();
                    loadDrawnCards();
                    makeBet();
                    chooseCard();
                    chooseCard();
                    startRound();
                    break;
                case "2":
                    jsonFile.loadJsonFile();
                    jsonFile.loadJsonObject();
                    jsonFile.loadJsonObjectRound();
                    loadDeck();
                    loadDrawnCards();
                    loadRoundAssets();
                    displayCards();
                    break;

            }
        }

        public void loadDeck()
        {
            numCardsUsed[0] = 0;

            usersHand[0] = 0;
            userBet = 0;
            isGameBet = false;
            winRound = false;


            string deck = jsonFile.getShuffledDeck();
            string useCardsString = jsonFile.getUsedCards();

            turn = 0;

            shuffledDeck = new string[52, 3];
            shuffleLongString = "";


            string[] deckSplit = deck.Split('|');
            
            //how many cards are in the deck
            for(int x = 0; x < deckSplit.Length - 1; x++)
            {
                //card (5,5,Spades)
                string cardAt = deckSplit[x];
                string[] cardParts = cardAt.Split(',');

                shuffledDeck[x, 0] = cardParts[0];
                shuffledDeck[x, 1] = cardParts[1];
                shuffledDeck[x, 2] = cardParts[2];

                
                shuffleLongString += cardParts[0] + "," + cardParts[1] + "," + cardParts[2] + "|";

            }

            string[] usedCardSplit = useCardsString.Split(',');
            usedCards = new string[52];

            for(int i = 0; i < usedCardSplit.Length - 1; i++)
            {
                usedCards[i] = usedCardSplit[i];
            }

        }

        public void loadDrawnCards()
        {
            drawCard = jsonFile.getDrawnCards();
            usersCash = jsonFile.getUsersCash();
        }

        public void loadRoundAssets()
        {
            numCardsUsed[0] = jsonFile.getTurnCardsUsed();

            string [] cardVals = jsonFile.getTurnCardVals().Split(',');

            for(int i = 0; i < cardVals.Length; i++)
            {
                if(cardVals[i] == null || cardVals[i] == "")
                {
                    usersUsedCards[0, i] = 0;
                }
                else
                {
                    usersUsedCards[0, i] = int.Parse(cardVals[i]);
                }
            }

            usersHand[0] = jsonFile.getUsersPoints();
            userBet = jsonFile.getBet();
            isGameBet = jsonFile.getIsGameBet();
            winRound = jsonFile.getWinRound();
            
        }

        public void makeBet()
        {
            Boolean isValid = true;
            do
            {
                Console.Clear();
                word.displayGameText();
                word.displayTableSetText();
           
                Console.WriteLine("\t\t\t\t\t\t You have $" + usersCash + " to play with");
                Console.WriteLine("\t\t\t\t\t\t Minimum Bet: 20");
            
                Console.Write("\t\t\t\t\t\t Player Bet: ");
                string userInput = Console.ReadLine();
                Boolean isNumber = validate.checkNum(userInput);
                if (isNumber == true)
                {
                    Console.Clear();
                    if (validate.checkFunds(usersCash) == false)
                    {
                        isValid = false;
                       
                    }
                    else
                    {
                        if (validate.checkBet(Int32.Parse(userInput), usersCash) == false)
                        {
                            isValid = false;
                        }
                        else
                        {
                            isValid = true;
                            userBet = Int32.Parse(userInput);
                        }
                    }
                }
                else
                {
                    isValid = false;
                    
                }

            } while (isValid == false);

        }

        public void startRound()
        {
            displayCards();
        }

        public void aiTurn()
        {
            chooseCard();
            displayCards();
        }

        public void aiCheck()
        {
            if(usersHand[1] <= 21 && numCardsUsed[1] <= 5)
            {
                if (usersHand[1] <= 21 && numCardsUsed[1] == 5)
                {
                    //this is where the house would stay but with a almost definite win
                    checkRoundWinner();
                }
                else if (usersHand[1] <= 15 && numCardsUsed[1] <= 5)
                {
                    chooseCard();
                    displayCards();
                }
                else if (usersHand[1] > 15 && numCardsUsed[1] <= 5)
                {
                    //this is where the house would stay and the comparison of the round would be made aware
                    checkRoundWinner();
                }
            }
            else
            {
                //this is where the house has gone over and the tally would be made
                checkRoundWinner();
            }
          
            

        }

        public void checkRoundWinner()
        {
            
            Thread.Sleep(2000);

            Console.Clear();
            
            //if both the house and the player has made it with a hand that is less than 21 run the below code
            if (usersHand[0] <= 21 && usersHand[1] <= 21)
            {
                if(numCardsUsed[0] == 5 && numCardsUsed[1] == 5)
                {
                    //tie no one wins
                    winner(3);
                }
                else if(numCardsUsed[0] == 5 && numCardsUsed[1] != 5)
                {
                    //the player wins
                    winner(0);
                }
                else if(numCardsUsed[0] != 5 && numCardsUsed[1] == 5)
                {
                    //house wins
                    winner(1);
                }

                if(usersHand[0] > usersHand[1])
                {
                    //the player wins
                    winner(0);
                }
                else if(usersHand[1] > usersHand[0])
                {
                    //the house wins
                    winner(1);
                }
                else if(usersHand[0] == usersHand[1])
                {
                    //tie (no one wins)
                    winner(3);
                }
            
            }
            else if(usersHand[0] <= 21 && usersHand[1] > 21)
            {
                //the player wins
                winner(0);
            }
            else if (usersHand[0] > 21 && usersHand[1] <= 21)
            {
                //the house wins
                winner(1);
            }
            else
            {
                //tie since both player and house went over
                winner(3);
            }

        }

        public void winner(int player)
        {
            word.displayGameText();

            if (player == 0)
            {
                winRound = true;
                document.checkNewHighBet(userBet, isGameBet);

                word.displayWinText();
                usersCash += userBet;
            }
            else if(player == 1)
            {
                word.displayLoseText();
                usersCash -= userBet;
            }
            else
            {
                word.displayTieText();
            }

            Console.WriteLine("\t\t\t\t\t\t Users Cash: " + usersCash);
            Console.WriteLine("\t\t\t\t\t\t Minimum Bet: " + "$20");

            Boolean correctInput = true;
            string userInput = "";
            do
            {
                Console.Write("\t\t\t\t\t\t Continue? - Y/N: ");
                userInput = Console.ReadLine();
                userInput = userInput.ToLower();

                if(userInput != "y" && userInput != "n" && userInput != "yes" && userInput != "no")
                {
                    correctInput = false;
                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    RemoveCurrentConsoleLine();
                }
                else
                {
                    correctInput = true;
                }

            } while (correctInput == false);
           
            if(userInput == "y" || userInput == "yes")
            {
                Console.Clear();
                resetForNextRound();
            }
            else
            {
                Console.Clear();

                word.displayGameText();
                word.displaySaveGameText();

                Boolean saveValid = false;
                string userInputSave = "";
                do
                {
                    Console.Write("\t\t\t\t\tWould you like to save where you are?(Y/N): ");
                    userInputSave = Console.ReadLine().ToLower();

                    if(userInputSave != "y" && userInputSave != "n")
                    {
                        Console.SetCursorPosition(0, Console.CursorTop - 1);
                        RemoveCurrentConsoleLine();
                        saveValid = false;
                    }
                    else
                    {
                        saveValid = true;
                    }

                } while (saveValid == false);

                if(userInputSave == "y")
                {
                    jsonFile.saveJsonFileRoundEnd(1, shuffleLongString, usedCards, drawCard, usersCash, 0, "0", 0,0,false,false);
                    shuffleDeck();

                    Console.Clear();

                    startGame();
                }
                else
                {
                    Boolean newHigh = document.checkHighscore(usersCash);

                    if (newHigh)
                    {
                        gameOver(0, document.getHighScore());
                    }
                    else
                    {
                        gameOver(1, document.getHighScore());
                    }
                }

            }
            
        }

        public void gameOver(int location, int highscore)
        {
            Console.Clear();
            word.displayGameText();

            word.displayGameOverText();
 
            Console.WriteLine("\t\t\t\t\t\t\tYou Left With " + usersCash + " Dollars.");
            if(location == 0)
            {
                Console.WriteLine("\t\t\t\t\t\tCongrats, this is your new highscore!!");
            }
            else
            {
                Console.WriteLine("\t\t\t\t\t\tHighscore to beat is: " + highscore);
            }

            if(document.getnewGameBet() == 1)
            {
                Console.WriteLine("\t\t\t\t\t\tYou have Set a New Highest Bet Placed in the Game: BET - " + document.getHighestBet());
            }
            else
            {
                Console.WriteLine("\t\t\t\t\t\tThe Highest Bet has not been broken: BET - " + document.getHighestBet());
            }

            Console.WriteLine("\t\t\t\t\t\t\tPress Enter to Restart...");
            Console.ReadLine();
            Console.Clear();

            cardDeck = new string[52, 3];
            shuffledDeck = new string[52, 3];
            usedCards = new string[52];
            drawCard = 0;

            usersUsedCards = new int[2, 5];
            outPut = new string[] { "", "" };
            numCardsUsed = new int[] { 0, 0 };
            usersHand = new int[] { 0, 0 };

            turn = 0;
            userBet = 0;

            isGameBet = false;
            winRound = false;

            shuffleDeck();
            startGame();
        }

        public void resetForNewGame()
        {
            cardDeck = new string[52, 3];
            shuffledDeck = new string[52, 3];
            usedCards = new string[52];
            drawCard = 0;

            usersUsedCards = new int[2, 5];
            outPut = new string[] { "", "" };
            numCardsUsed = new int[] { 0, 0 };
            usersHand = new int[] { 0, 0 };

            turn = 0;
            userBet = 0;

            isGameBet = false;
            winRound = false;

            cardDeck = new string[52, 3];
            shuffledDeck = new string[52, 3];
            usedCards = new string[52];           

            fillDeck();
            drawCard = 0;
        }

        public void resetForNextRound()
        {
            if (drawCard >= 25)
            {
                cardDeck = new string[52, 3];
                shuffledDeck = new string[52, 3];
                usedCards = new string[52];


                fillDeck();
                drawCard = 0;
            }


            usersUsedCards = new int[2, 5];
            outPut = new string[] { "", "" };
            numCardsUsed = new int[] {0,0};
            usersHand = new int[] { 0, 0 };

            turn = 0;
            userBet = 0;
            winRound = false;

            makeBet();

            chooseCard();
            chooseCard();

            displayCards();
        }

       

        public void userAction()
        {
            Boolean isTrue = false;
            string userInput = "";
            do
            {
                string option = centering + "What Would you like to do:  ";
                Console.Write(option);
                userInput = Console.ReadLine();

                userInput = userInput.ToLower();
                if(userInput != "hit me" && userInput != "stay" && userInput != "save" && userInput != "mm")
                {
                    isTrue = true;

                    Console.SetCursorPosition(0, Console.CursorTop - 1);
                    RemoveCurrentConsoleLine();

                }
                else
                {
                    isTrue = false;
                }
            } while (isTrue == true);


            
            if(userInput == "hit me")
            {
                chooseCard();
                startRound();
            }
            else if(userInput == "stay")
            {
                //this is when it will be the houses turn to pick up cards
                turn = 1;
                aiTurn();
            }
            else if(userInput == "mm")
            {
                Console.Clear();
                resetForNewGame();
                startGame();
            }
            else
            {
     
                string uuc = usersUsedCards[0, 0].ToString() + "," + usersUsedCards[0, 1].ToString() + "," + usersUsedCards[0, 2].ToString() + "," +
                    usersUsedCards[0, 3].ToString() + "," + usersUsedCards[0, 4].ToString();

               
                //save the current state of the game
                jsonFile.saveJsonFileRoundEnd(2, shuffleLongString, usedCards, drawCard, usersCash,numCardsUsed[0],uuc, usersHand[0], userBet, isGameBet, winRound);
                shuffleDeck();
                Console.Clear();            
                startGame();

                
            }
            Console.ReadLine();

        }

        public static void RemoveCurrentConsoleLine()
        {
            int currentCursorLine = Console.CursorTop;
            Console.SetCursorPosition(0, Console.CursorTop);
            Console.Write(new string(' ', Console.WindowWidth));
            Console.SetCursorPosition(0, currentCursorLine);
        }

        public void displayCards()
        {
            Console.Clear();
            //new way to populate the console
            string line1 = "";
            string line2 = "";
            string line3 = "";
            string line4 = "";
            string line5 = "";

            string length1 = "                    ";
            string length2 = "              ";
            string length3 = "                    ";
            string length4 = "          ";

            int[] lengths = { 20, 14, 20, 10 };

            string difference = "";

            if (usersHand[turn] < 10)
            {
                //difference will be same

                length4 = length4 + " ";
                
            }

           

            string user = "";
            string underline = "";
            if(turn == 0)
            {
                user = centering + "PLAYER";
                underline = centering + "------";
            }
            else
            {
                user = centering + "HOUSE ";
                underline = centering + "----- ";
            }

            //for each card the user has drawn re run the below code
            for (int i = 0; i < numCardsUsed[turn]; i++)
            {
                string card = shuffledDeck[usersUsedCards[turn, i], 0][0].ToString() + shuffledDeck[usersUsedCards[turn, i], 2][0] + " ";
                if (shuffledDeck[usersUsedCards[turn, i], 0] == "10")
                {
                    card = shuffledDeck[usersUsedCards[turn, i], 0].ToString() + shuffledDeck[usersUsedCards[turn, i], 2][0];
                }

                if (i == 0)
                {
                    //20 spaces
                    //line 2 are at 14
                    //line 3 is at 14
                    line1 += length1 + centering + "+--------+";
                    line2 += length2 + "|" + card + "     |";
                    line3 += length2 + "|        |";
                    line4 += length3 + "|     " + card + "|";
                    line5 += length4 + "|        |";
                }
                else
                {
                    line1 += "    +--------+";
                    line2 += "    |" + card + "     |";
                    line3 += "    |        |";
                    line4 += "    |     " + card + "|";
                    line5 += "    " + "|        |";

                }
                
            }

            string cardShow = line1 + "\n" + user + line2 + "\n" + underline + line3 + "\n" + centering + "POINTS: " + usersHand[turn] + line5 + "\n" + centering + line4 + "\n" + line1;

            if(turn == 0)
            {
                outPut[turn] = cardShow + "\n\n" +
                               centering + glossary + "                    Options" + "\n" +
                               centering + "PlayerBet: " + userBet + glossary + "         -------" + "\n" +
                               centering + "Player's Cash: " + usersCash + glossary + "    HIT ME" + "\n" +
                               centering + glossary + "                      STAY" + "\n" +
                               centering + glossary + glossary +  "       SAVE \n" +
                               centering + glossary + glossary + "       MAIN MENU(MM)";
                word.displayGameText();

                Console.WriteLine(outPut[turn]);


                if(numCardsUsed[turn] <= 5 && usersHand[turn] == 21)
                {
                    turn = 1;
                    aiTurn();
                }
                if (numCardsUsed[turn] < 5 && usersHand[turn] <= 21)
                {
                    userAction();
                }
                else if (numCardsUsed[turn] == 5 && usersHand[turn] <= 21)
                {
                    //Console.WriteLine("You have 5 cards and have a point total of 21 or less, you win!");
                    turn = 1;
                    aiTurn();
                }
                else if (usersHand[turn] > 21)
                {
                    Console.WriteLine("You are over 21!");
                    turn = 1;
                    aiTurn();
                }

            }
            else if(turn == 1)
            {
                outPut[turn] = cardShow;

                word.displayGameText();
                Console.WriteLine(outPut[1]);
                Console.WriteLine(outPut[0]);

                aiCheck();
            }


            
        }

        public void chooseCard()
        {
            if(turn == 1 && numCardsUsed[turn] >= 1)
            {
                Thread.Sleep(1000);
            }

            usedCards[drawCard] = drawCard.ToString();
            usersUsedCards[turn, numCardsUsed[turn]] = drawCard;
            numCardsUsed[turn] = numCardsUsed[turn] + 1;

            Boolean findAce = false;
            int totalNum = 0;

            for (int i = 0; i < numCardsUsed[turn]; i++)
            {
                if(shuffledDeck[usersUsedCards[turn, i], 0] == "ACE")
                {
                    findAce = true;
                }
                else
                {
                    totalNum += int.Parse(shuffledDeck[usersUsedCards[turn, i], 1]);
                }          
            }


            int trialNum = totalNum;
            if (findAce)
            {
                if(trialNum + 11 <= 21)
                {
                    totalNum = totalNum + 11;
                }
                else
                {
                    totalNum = totalNum + 1;
                }
            }

            usersHand[turn] = totalNum;
            

            drawCard++;

        }

        public int getRandomNum(int start, int end)
        {
            Random rnd = new Random();
            int ranNum = rnd.Next(start, end);
            return ranNum;
        }


        /*The below two methods are used to fill the deck and then randomely select cards from that new deck */
        public void fillDeck()
        {

            string[] cardName = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "JACK", "QUEEN", "KING", "ACE" };
            string[] cardValue = { "2", "3", "4", "5", "6", "7", "8", "9", "10", "13"};
            string[] cardSuite = { "HEARTS", "DIAMONDS", "SPADES", "CLUBS" };

            shuffledDeck = new string[52, 3];

            int counter = 0;
            for (int x = 0; x < cardSuite.Length; x++)
            {
                for(int y = 0; y < cardName.Length; y++)
                {
                    //dealing with an ace
                    if (y == 12)
                    {
                        shuffledDeck[counter, 0] = cardName[y];
                        shuffledDeck[counter, 1] = cardValue[9];
                        shuffledDeck[counter, 2] = cardSuite[x];

                        counter++;
                    }
                    //dealing with 10, Jack, Queen, King
                    else if (y > 7)
                    {
                        shuffledDeck[counter, 0] = cardName[y];
                        shuffledDeck[counter, 1] = cardValue[8];
                        shuffledDeck[counter, 2] = cardSuite[x];

                        counter++;
                    }
                    else
                    {
                        shuffledDeck[counter, 0] = cardName[y];
                        shuffledDeck[counter, 1] = cardValue[y];
                        shuffledDeck[counter, 2] = cardSuite[x];

                        counter++;
                    }

                }
                    
            }

            shuffleDeck();
        }

        public void shuffleDeck()
        {
            shuffleLongString = "";
            Random rand = new Random();

            for(int i = 0; i < shuffledDeck.GetLength(0); i++)
            {
                string temp1 = shuffledDeck[i,0];
                string temp2 = shuffledDeck[i, 1];
                string temp3 = shuffledDeck[i, 2];

                int r = rand.Next(0, 52);

                shuffledDeck[i, 0] = shuffledDeck[r, 0];
                shuffledDeck[i, 1] = shuffledDeck[r, 1];
                shuffledDeck[i, 2] = shuffledDeck[r, 2];

                shuffledDeck[r, 0] = temp1;
                shuffledDeck[r, 1] = temp2;
                shuffledDeck[r, 2] = temp3;
            }

            for(int x = 0; x < shuffledDeck.GetLength(0); x++)
            {
                shuffleLongString += shuffledDeck[x,0] + "," + shuffledDeck[x, 1] + "," + shuffledDeck[x, 2] + "|";
            }
            
        }

    }
}
