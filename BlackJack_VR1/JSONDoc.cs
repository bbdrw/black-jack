﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackJack_VR1
{
    class JSONDoc
    {
        JObject dict;

        string saveType;

        string deck;
        string usedCards;
        int drawnCards = 0;
        int usersCash = 0;

        int cardsUsed = 0;
        string turnCardVals = "";
        int usersPoints = 0;
        int userBet = 0;

        Boolean isGameBet = false;
        Boolean winRound = false;
         

        public JSONDoc()
        {
            loadJsonFile();
        }

        public void loadJsonFile()
        {
            try
            {
                String file = File.ReadAllText(@"..\..\files\saveFile.json");
                dict = JObject.Parse(file);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        public Boolean checkSaveFile()
        {
            //Console.WriteLine(dict["typeOfSave"].ToString());

            if(dict["typeOfSave"].ToString() == "0")
            {
                return false;
            }

            setSaveType(dict["typeOfSave"].ToString());
            return true;
        }

        public string getSaveType()
        {
            return saveType;
        }

        public void setSaveType(string saveType)
        {
            this.saveType = saveType;
        }

        public void saveJsonFileRoundEnd(int saveType, string shuffledDeck, string[] usedCards, int drawCard, int usersCash, int turnCardsUsed, string turnCardsValues, int usersPoints, int bet, Boolean isGameBet, Boolean winRound)
        {
            string newUsedCards = "";
            for(int i = 0; i < usedCards.Length; i++)
            {
                newUsedCards += usedCards[i] + ",";
            }         


            string json = File.ReadAllText(@"..\..\files\saveFile.json");
            dynamic DynamicObject = JsonConvert.DeserializeObject<dynamic>(json);
            DynamicObject.typeOfSave= saveType;
            DynamicObject.shuffledDeck = shuffledDeck;
            DynamicObject.usedCards = newUsedCards;
            DynamicObject.drawnCards = drawCard;
            DynamicObject.player.usersCash = usersCash;
          
            if(saveType == 2)
            {
                //add the additonal lines
                DynamicObject.turn.turnUsedCards = turnCardsUsed;
                DynamicObject.turn.turnUsedCardVals = turnCardsValues;
                DynamicObject.turn.usersHand = usersPoints;
                DynamicObject.turn.bet = bet;
                DynamicObject.turn.isGameBet = isGameBet;
                DynamicObject.turn.winRound = winRound;

            }

            string serJson = JsonConvert.SerializeObject(DynamicObject);


            System.IO.File.WriteAllText(@"..\..\files\saveFile.json", serJson);

        }

        public void setShuffledDeck(string shuffled)
        {
            deck = shuffled;
        }

        public string getShuffledDeck()
        {
            return deck;
        }

        public void setUsedCards(string usedCards)
        {
            this.usedCards = usedCards;
        }


        public string getUsedCards()
        {
            return usedCards;
        }

        public void setDrawnCards(int drawnCards)
        {
            this.drawnCards = drawnCards; 
        }

        public int getDrawnCards()
        {
            return drawnCards;
        }

        public void setUsersCash(int usersCash)
        {
            this.usersCash = usersCash;
        }

        public int getUsersCash()
        {
            return usersCash;
        }

        public void setTurnCardsUsed(int cardsUsed)
        {
            this.cardsUsed = cardsUsed;
        }

        public int getTurnCardsUsed()
        {
            return cardsUsed;
        }

        public void setTurnCardVals(string turnCardVals)
        {
            this.turnCardVals = turnCardVals;
        }

        public string getTurnCardVals()
        {
            return turnCardVals;
        }


        public void setUsersPoints(int usersPoints)
        {
            this.usersPoints = usersPoints;
        }

        public int getUsersPoints()
        {
            return usersPoints;
        }

        public void setBet(int userBet)
        {
            this.userBet = userBet;
        }

        public int getBet()
        {
            return userBet;
        }

        public void setisGameBet(Boolean isGameBet)
        {
            this.isGameBet = isGameBet;
        }

        public Boolean getIsGameBet()
        {
            return isGameBet;
        }

        public void setwinRound(Boolean winRound)
        {
            this.winRound = winRound;
        }

        public Boolean getWinRound()
        {
            return winRound;
        }

        public void loadJsonObject()
        {
            setShuffledDeck(dict["shuffledDeck"].ToString());
            setUsedCards(dict["usedCards"].ToString());
            setDrawnCards(int.Parse(dict["drawCard"].ToString()));
            setUsersCash(int.Parse(dict["player"]["usersCash"].ToString()));

        }

        public void loadJsonObjectRound()
        {
            setTurnCardsUsed(int.Parse(dict["turn"]["turnUsedCards"].ToString()));
            setTurnCardVals(dict["turn"]["turnUsedCardVals"].ToString());
            setUsersPoints(int.Parse(dict["turn"]["usersHand"].ToString()));
            setBet(int.Parse(dict["turn"]["bet"].ToString()));
            setisGameBet(Boolean.Parse(dict["turn"]["isGameBet"].ToString()));
            setwinRound(Boolean.Parse(dict["turn"]["winRound"].ToString()));
        }

        
    }
}
