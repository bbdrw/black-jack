﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BlackJack_VR1
{
    class XmlDoc
    {
        XmlDocument myfile;
        int newGameBet = 0;
        int highestBet = 0;
        int highScoreSave = 0;

        string highScores = "";

        public XmlDoc()
        {
            myfile = new XmlDocument();
            myfile.Load(@"..\..\files\blackjack.xml");

            setHighScore(myfile);
            setHighBet(myfile);
           
        }


        public Boolean checkHighscore(int usersCash)
        {
            int highscore = 0;

            if (highscore < usersCash)
            {
                //new highscore
                changeHighscore(usersCash);
                highScoreSave = usersCash;
                return true;
            }
            else
            {
                highScoreSave = highscore;
                return false;
            }

        }

        public void changeHighscore(int highscore)
        {
            XmlDocument myDoc = new XmlDocument();

            myDoc.Load(@"..\..\files\blackjack.xml");

            myDoc.SelectSingleNode("Game/Player/HighScore").InnerText = highscore.ToString();

            // XmlNode node = myDoc.SelectSingleNode("Root/node/Element");
            //node.Attributes[0].Value = highscore.ToString();

            myDoc.Save(@"..\..\files\blackjack.xml");
        }

        public void checkNewHighBet(int bet, Boolean isGameBet)
        {

            int highBet = highestBet;

            if (highBet < bet)
            {
             
                newGameBet = 1;
                highestBet = bet;
                changeHighBet(bet);

            }
            else
            {
                if (isGameBet == false)
                {
                    highestBet = highBet;
                    newGameBet = 0;
                }

            }

        }

        public void changeHighBet(int newBet)
        {
            XmlDocument myDoc = new XmlDocument();

            myDoc.Load(@"..\..\files\blackjack.xml");

            myDoc.SelectSingleNode("Game/Player/HighBet").InnerText = newBet.ToString();

            myDoc.Save(@"..\..\files\blackjack.xml");
        }

        public string getHighScores()
        {
            //used for highscore screen
            return highScores;
        }

        public void setHighScore(XmlDocument file)
        {
            XmlNodeList elemList = myfile.GetElementsByTagName("HighScore");
            for (int i = 0; i < elemList.Count; i++)
            {
                highScoreSave = int.Parse(elemList[i].InnerXml);
            }
        }

        public void setHighBet(XmlDocument file)
        {
            highestBet = int.Parse(file.SelectSingleNode("Game/Player/HighBet").InnerText);
        }

        public int getHighestBet()
        {
            return highestBet;
        }

        public int getnewGameBet()
        {
            return newGameBet;
        }

        public int getHighScore()
        {
            return highScoreSave;
        }


    }
}
